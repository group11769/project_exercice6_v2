      ******************************************************************
      * Program name:    Exercice                                       
      *                                                                 
      * Original author: O. AZOUZ.                                      
      *                                                                 
      * Purpose :                                                       
      *    Consultation du programme tele de la semaine                 
      *                                                                 
      * Using :                                                         
      *    - Copybooks : None                                           
      *    - File      : semaine.txt                                    
      *                                                                 
      * Returning :                                                     
      *    None                                                         
      *                                                                 
      * Realized with :                                                 
      *    - Personal Cobol (Microfocus)                                
      * Maintenance Log                                                 
      * Date      Author   Maintenance Requirement                      
      * --------- -------- ---------------------------------------      
      * 08/02/98  OAZOUZ   Created for practice                         
      * 23/12/22  EDEFAY   Documentation                                
      *                                                                 
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.   Exercice.
       AUTHOR.       O. AZOUZ.
       DATE-WRITTEN. 08/02/1998.      

      ******************************************************************EDEFAY
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT F-FILMS
           ASSIGN TO "semaine.txt"
           ORGANIZATION LINE SEQUENTIAL.

      ******************************************************************EDEFAY
       DATA DIVISION.
       FILE SECTION.

       FD F-FILMS.
       01 F-ENR.
           02 JOUR       PIC XXX.
           02 CHAINE     PIC XXXX.
      *    heure de debut et duree en minutes
           02 HHMM       PIC 9999.
           02 DUREE      PIC 999.
      *    titre
           02 TITRE      PIC X(60).

       WORKING-STORAGE SECTION.
      ******************************************************************EDEFAY
      * constantes definies par commodite.
       77  VRAI          PIC X   VALUE "V".
       77  FAUX          PIC X   VALUE "F".

       77  FIN-MENU      PIC X.
       77  FIN-FICHIER   PIC X.
      
       77  CHOIX-MENU    PIC X.

       01  LIGNE-AFFICHAGE.
           02            PIC XX  VALUE "* ".
           02 JOUR       PIC XXX.
           02            PIC X   VALUE " ".
           02 CHAINE     PIC X(04).
           02            PIC XXX VALUE " | ".
           02 HHMM       PIC Z9B99.
           02            PIC X   VALUE " ".
           02 TITRE      PIC X(30).
           02            PIC XX  VALUE " |".

       01  LIGNE-ETOILES PIC X(50) VALUE ALL "*".
      
       77  JOUR-CHOISI   PIC X(03).
       77  NB-FILMS      PIC 999.


      ******************************************************************EDEFAY
       PROCEDURE DIVISION.
       PRINCIPALE SECTION.

       DEBUT.
      ******************************************************************EDEFAY
      *  Cette routine doit initialiser des valeurs et lancer le prog   EDEFAY
      *  principal.                                                     EDEFAY
           DISPLAY "hello"
           MOVE FAUX TO FIN-MENU
           PERFORM MENU UNTIL FIN-MENU = VRAI
           STOP RUN
           .

       MENU.
             display "*************************************"
             display "* Films de la semaine               *"
             display "* --------------------              *".
             display "* T. Tous les films                 *"
             display "* J. Films d'un jour                *"
             display "* C. Films d'une chaine             *"
             display "* H. Recherche par heure de debut   *"
             display "* Q. Quitter                        *".
             display "*                                   *"
             display "*************************************"
             display "Votre choix ?"
             
             accept CHOIX-MENU
             
             evaluate CHOIX-MENU
             when "T"
             when "t"
                   perform EDITER-TOUS-FILMS
             when "J"
             when "j"
                   perform EDITER-JOUR
             when "C"
             when "c"
                   perform EDITER-CHAINE
             when "H"
             when "h"
                   perform RECHERCHER-HORAIRE
             when "Q"
             when "q"
                   move VRAI to FIN-MENU
             when other
                   display "Choix invalide"
             end-evaluate.

       EDITER-TOUS-FILMS.
      ******************************************************************EDEFAY
      *  Cette routine doit afficher tous les films du fichier initial  EDEFAY
           MOVE FAUX TO FIN-FICHIER
           OPEN INPUT F-FILMS
           DISPLAY LIGNE-ETOILES
           MOVE 0 TO NB-FILMS
           PERFORM UNTIL FIN-FICHIER = VRAI
              MOVE SPACES TO F-ENR
              READ F-FILMS
              AT END
                 MOVE VRAI TO FIN-FICHIER
              NOT AT END
                 ADD 1 TO NB-FILMS
                 PERFORM AFFICHER-ENREGISTREMENT
              END-READ
           END-PERFORM.
           CLOSE F-FILMS
           DISPLAY LIGNE-ETOILES
           DISPLAY NB-FILMS " films trouves."
           DISPLAY " "
           .

       AFFICHER-ENREGISTREMENT.
      ******************************************************************EDEFAY
      *  Cette routine affiche une ligne de texte a l'ecran             EDEFAY
           MOVE CHAINE OF F-ENR TO CHAINE OF LIGNE-AFFICHAGE
           MOVE JOUR   OF F-ENR TO JOUR   OF LIGNE-AFFICHAGE
           MOVE HHMM   OF F-ENR TO HHMM   OF LIGNE-AFFICHAGE
           MOVE TITRE  OF F-ENR TO TITRE  OF LIGNE-AFFICHAGE

           DISPLAY LIGNE-AFFICHAGE
           .

       EDITER-JOUR.
      ******************************************************************EDEFAY
      *  Cette routine doit afficher les films du jour voulu            EDEFAY
           DISPLAY "Quel Jour ? (3 premieres lettres majuscules)"
           ACCEPT JOUR-CHOISI
      
           MOVE FAUX TO FIN-FICHIER
           OPEN INPUT F-FILMS
           MOVE 0 TO NB-FILMS
           DISPLAY LIGNE-ETOILES
000135     PERFORM UNTIL FIN-FICHIER = VRAI
              MOVE SPACES TO F-ENR
              READ F-FILMS
              AT END
                 MOVE VRAI TO FIN-FICHIER
000140        NOT AT END
                 IF JOUR-CHOISI = JOUR OF F-ENR
                 THEN
                   ADD 1 TO NB-FILMS
                   PERFORM AFFICHER-ENREGISTREMENT
000145           END-IF
              END-READ
           END-PERFORM
           CLOSE F-FILMS
           DISPLAY LIGNE-ETOILES
000150     DISPLAY NB-FILMS " films trouves pour le jour "
                   JOUR-CHOISI
           DISPLAY " "
           .

        EDITER-CHAINE.
      ******************************************************************EDEFAY
      *  Cette routine doit afficher les films de la chaine voulue      EDEFAY
000155     DISPLAY "* EDITER-CHAINE : A completer"
           .

        RECHERCHER-HORAIRE.
      ******************************************************************EDEFAY
      *  Cette routine doit rechercher les films de l'horaire voulue    EDEFAY
           DISPLAY "* RECHERCHER-HORAIRE : A completer"
           .